"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Computes the average value given a dice of `size`
 *
 * @param {number} size The number of sides on a die
 * @returns {number} The average value.
 */

var averageRoll = function averageRoll(size) {
  return (size + 1) / 2;
};

exports.default = averageRoll;