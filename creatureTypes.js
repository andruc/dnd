'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of creature types in D&D.
 */

var creatureTypes = ['aberration', 'beast', 'celestial', 'construct', 'dragon', 'elemental', 'fey', 'fiend', 'giant', 'humanoid', 'monstrosity', 'ooze', 'plant', 'undead'];

exports.default = creatureTypes;