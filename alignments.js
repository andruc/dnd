'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of possible individual creature alignments
 */

var alignments = ['any alignment', 'any non-lawful alignment', 'any non-good alignment', 'any evil alignment', 'any chaotic alignment', 'any lawful alignment', 'unaligned', 'lawful evil', 'lawful neutral', 'lawful good', 'neutral evil', 'neutral', 'neutral good', 'chaotic evil', 'chaotic neutral', 'chaotic good'];

exports.default = alignments;