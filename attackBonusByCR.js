"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A function that computes the expected attack bonus for
 * a given challenge rating
 *
 * @param {number} cr A Challenge Rating INDEX
 * @return {number} Expected attack bonus
 * @example
 *
 * attackBonusByCR(1) // CR 1/8
 * // => 3
 *
 * attackBonusByCR(12) // CR 8
 * // => 7
 */

var attackBonusByCR = function attackBonusByCR(cr) {
  var cri = cr + 1;

  return Number(cri <= 6 ? 3 : cri <= 7 ? 4 : cri <= 8 ? 5 : cri <= 11 ? 6 : cri <= 14 ? 7 : cri <= 19 ? 8 : cri <= 20 ? 9 : cri <= 24 ? 10 : cri <= 27 ? 11 : cri <= 30 ? 12 : cri <= 33 ? 13 : 14);
};

exports.default = attackBonusByCR;