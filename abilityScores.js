'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of every ability score in D&D
 */

var abilityScores = ['strength', 'dexterity', 'constitution', 'intelligence', 'wisdom', 'charisma'];

exports.default = abilityScores;