'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _crToIndex = require('./_crToIndex');

var _crToIndex2 = _interopRequireDefault(_crToIndex);

var _proficiencyByCR = require('./proficiencyByCR');

var _proficiencyByCR2 = _interopRequireDefault(_proficiencyByCR);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var calc = function calc(creature) {
  var modCR = creature.modified_challenge_rating;
  var CR = creature.challenge_rating;

  var cri = (0, _crToIndex2.default)(modCR || CR);

  return {
    proficiency: function proficiency() {
      return (0, _proficiencyByCR2.default)(cri);
    }
  };
};

exports.default = calc;