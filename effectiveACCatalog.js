'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _abilityMod = require('./abilityMod');

var _abilityMod2 = _interopRequireDefault(_abilityMod);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * A collection of functions that compute effective armor class
 * increases for various traits and actions in the SRD
 *
 * Exports a higher-order function called to look up the
 * armor class adjustment
 *
 * @param {object} creature A creature object
 * @returns {function} Returns a function that takes a trait and
 *  returns a number
 *
 * @param {object} feature A trait or action object
 * @returns {number} Returns an effective armor class value
 */

/**
 * Functions that return a flat value.
 */
var increaseByOne = function increaseByOne() {
  return 1;
};
var increaseByTwo = function increaseByTwo() {
  return 2;
};
var increaseByFour = function increaseByFour() {
  return 4;
};

/**
 * Functions that return a value equal to an ability modifier
 */
var addAbilityModifer = function addAbilityModifer(score) {
  return (0, _abilityMod2.default)(score);
};

var eacCalculations = {
  'avoidance': increaseByOne,
  'constrict': increaseByOne,
  'cunning action': increaseByFour,
  'fiendish blessing': function fiendishBlessing(c) {
    return addAbilityModifer(c.charisma);
  },
  'magic resistance': increaseByTwo,
  'parry': increaseByOne,
  'psychic defense': function psychicDefense(c) {
    return addAbilityModifer(c.wisdom);
  },
  'nimble escape': increaseByFour,
  'shadow stealth': increaseByFour,
  'stench': increaseByOne,
  'superior invisibility': increaseByFour,
  'web (recharge 5-6)': increaseByOne
};

var effectiveACCatalog = function effectiveACCatalog(creature, feature) {
  var name = feature.name.toLowerCase();
  var calc = eacCalculations[name];

  if (feature.effect && feature.effect.ac) {
    return feature.effect.ac;
  }

  return calc ? calc(creature, feature) : 0;
};

exports.default = effectiveACCatalog;