'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _concat = require('lodash/concat');

var _concat2 = _interopRequireDefault(_concat);

var _reduce = require('lodash/reduce');

var _reduce2 = _interopRequireDefault(_reduce);

var _add = require('lodash/add');

var _add2 = _interopRequireDefault(_add);

var _crToIndex = require('./_crToIndex');

var _crToIndex2 = _interopRequireDefault(_crToIndex);

var _criToCR = require('./_criToCR');

var _criToCR2 = _interopRequireDefault(_criToCR);

var _defensiveCR = require('./defensiveCR');

var _defensiveCR2 = _interopRequireDefault(_defensiveCR);

var _effectiveHPCatalog = require('./effectiveHPCatalog');

var _effectiveHPCatalog2 = _interopRequireDefault(_effectiveHPCatalog);

var _effectiveACCatalog = require('./effectiveACCatalog');

var _effectiveACCatalog2 = _interopRequireDefault(_effectiveACCatalog);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * A function that computes a creature's challenge rating
 *
 * @param {object} creature A creature object as defined by dnd5eapi.co
 * @return {number} Returns a challenge rating in decimal form
 */

var estimateCR = function estimateCR(creature) {
  var ocri = null;

  var features = (0, _concat2.default)(creature.actions, creature.special_abilities, creature.reactions).filter(function (s) {
    return !!s;
  });

  var HP = creature.hit_points;
  var AC = creature.armor_class;

  var eHPLookup = (0, _effectiveHPCatalog2.default)(creature);
  var eACLookup = (0, _effectiveACCatalog2.default)(creature);

  var effectiveHP = HP + (0, _reduce2.default)(features.map(eHPLookup), _add2.default);

  var effectiveAC = AC + (0, _reduce2.default)(features.map(eACLookup), _add2.default);

  var dcri = (0, _crToIndex2.default)((0, _defensiveCR2.default)(effectiveHP, effectiveAC));

  // When we can calculate OCR, replace this
  if (creature.offensive_challenge_rating) {
    ocri = (0, _crToIndex2.default)(creature.offensive_challenge_rating);
  } else {
    ocri = creature.challenge_rating * 2 - dcri;
  }

  return {
    challenge_rating: (0, _criToCR2.default)(Math.floor((dcri + ocri) / 2)),
    offensive_challenge_rating: (0, _criToCR2.default)(ocri),
    defensive_challenge_rating: (0, _criToCR2.default)(dcri)
  };
};

exports.default = estimateCR;