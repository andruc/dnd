'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of movement types
 */

var speeds = ['walk', 'burrow', 'climb', 'fly', 'swim'];

exports.default = speeds;