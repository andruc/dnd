'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of resistances that aren't damage types or weapons
 */

var specialResistances = ['damage from spells'];

exports.default = specialResistances;