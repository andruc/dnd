"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Creates a function that takes an ability score and
 * computes `modifier`.
 *
 * @param {number} score The base ability score
 * @returns {number} Returns the dice modifier
 */

var abilityMod = function abilityMod(score) {
  return Math.floor((score - 10) / 2);
};

exports.default = abilityMod;