'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _reactToastify = require('react-toastify');

var _dnd = require('../modules/dnd');

var _dnd2 = _interopRequireDefault(_dnd);

var _challegeRating = require('../modules/challegeRating');

var _challegeRating2 = _interopRequireDefault(_challegeRating);

var _creatureCalculator = require('../modules/creatureCalculator');

var _creatureCalculator2 = _interopRequireDefault(_creatureCalculator);

var _Statblock = require('../Statblock');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var size = function size(base, _size) {

  var hdCount = Number(base.hit_dice.split('d')[0]);
  var hdSize = _dnd2.default.size_hit_dice[_size];

  var hpFromHD = Math.floor(hdCount * (0, _Statblock.average)(hdSize));
  var hpFromCon = hdCount * (0, _Statblock.modValue)(base.constitution);

  var modifiedHD = hdCount + 'd' + hdSize;
  var modifiedHP = hpFromHD + hpFromCon;

  if (modifiedHP > _challegeRating.maxHP) {
    // warn user
    (0, _reactToastify.toast)('Max HP is too high. CR may be invalid.');
  }

  var newCreature = _extends({}, base, {
    size: _size,
    modified_hit_dice: modifiedHD,
    modified_hit_points: modifiedHP
  });

  newCreature.modified_challenge_rating = _challegeRating2.default.estimate(newCreature);

  return newCreature;
};

var abilityUpdater = function abilityUpdater(ability, then) {
  return function (base, value) {
    var creature = _extends({}, base, _defineProperty({}, ability, value));
    return then(creature);
  };
};

var recalculateSkills = function recalculateSkills(c, ability) {
  var prof = (0, _creatureCalculator2.default)(c).proficiency();

  Object.keys(c).filter(function (s) {
    return _dnd2.default.skills.includes(s) && _dnd2.default.skill_abilities[s] === ability;
  }).forEach(function (s) {
    return c[s] = prof + (0, _Statblock.modValue)(c[ability]);
  });

  return c;
};

var recalculateSaves = function recalculateSaves(c, save) {
  var prof = (0, _creatureCalculator2.default)(c).proficiency();

  if (c[save + '_save']) {
    c[save + '_save'] = prof + (0, _Statblock.modValue)(c[save]);
  }

  return c;
};

var strength = abilityUpdater('strength', function (c) {
  recalculateSkills(c, 'strength');
  recalculateSaves(c, 'strength');
  return c;
});

var dexterity = abilityUpdater('dexterity', function (c) {
  recalculateSkills(c, 'dexterity');
  recalculateSaves(c, 'dexterity');
  return c;
});

var constitution = abilityUpdater('constitution', function (c) {
  c.modified_hit_points = _dnd2.default.hit_points(c);
  recalculateSkills(c, 'constitution');
  recalculateSaves(c, 'constitution');
  c.modified_challenge_rating = _challegeRating2.default.estimate(c);
  return c;
});

var intelligence = abilityUpdater('intelligence', function (c) {
  recalculateSkills(c, 'intelligence');
  recalculateSaves(c, 'intelligence');
  return c;
});

var wisdom = abilityUpdater('wisdom', function (c) {
  var prof = (0, _creatureCalculator2.default)(c).proficiency();

  recalculateSkills(c, 'wisdom');
  recalculateSaves(c, 'wisdom');

  var pp = 10 + (0, _Statblock.modValue)(c.wisdom) + (c.perception ? prof : 0);

  c.senses = c.senses.replace(/passive Perception \d*/, 'passive Perception ' + pp);

  return c;
});

var charisma = abilityUpdater('charisma', function (c) {
  recalculateSkills(c, 'charisma');
  recalculateSaves(c, 'charisma');
  return c;
});

exports.default = {
  size: size,
  strength: strength,
  dexterity: dexterity,
  constitution: constitution,
  intelligence: intelligence,
  wisdom: wisdom,
  charisma: charisma
};