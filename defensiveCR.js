'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _criToCR = require('./_criToCR');

var _criToCR2 = _interopRequireDefault(_criToCR);

var _hitPointsByCR = require('./hitPointsByCR');

var _hitPointsByCR2 = _interopRequireDefault(_hitPointsByCR);

var _armorClassByCR = require('./armorClassByCR');

var _armorClassByCR2 = _interopRequireDefault(_armorClassByCR);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * A function that computes a creature's defensive challenge
 * rating index
 *
 * @param {number} eHP A creature's Effective Hit Points
 * @param {number} eAC A creature's Effective Armor Class
 * @return {number} Returns the defensive challenge rating
 */

var defensiveCR = function defensiveCR(eHP, eAC) {
  var hpIndex = _hitPointsByCR2.default.indexOf(_hitPointsByCR2.default.find(function (cr) {
    return eHP >= cr[0] && eHP <= cr[1];
  }));

  // find what our expected AC should be for this DCR
  var targetAC = (0, _armorClassByCR2.default)(hpIndex);

  // for every 2 AC that the actual differs from the
  // target, we adjust the final DCR by 1
  var finalDCRI = hpIndex + Math.trunc((eAC - targetAC) / 2);

  // make sure DCR doesn't go negative
  finalDCRI = Math.max(finalDCRI, 0);

  return (0, _criToCR2.default)(finalDCRI);
};

exports.default = defensiveCR;