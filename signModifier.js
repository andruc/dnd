"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Returns a signed number as a string, using the precendent
 * for displaying D&D Ability Modifiers
 *
 * @param {number} modifier The number to sign
 * @returns {string} Returns the stringified modifier
 * @example
 *
 * signModifier(4)
 * // => "+4"
 *
 * signModifier(0)
 * // => "+0"
 *
 * signModifier(-3)
 * // => "-3"
 */

var signModifier = exports.signModifier = function signModifier(modifier) {
  return modifier < 0 ? String(modifier) : "+" + String(modifier);
};