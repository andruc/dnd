'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of creature sizes objects
 */

var sizes = [{ name: 'tiny',
  hitDie: 4 }, { name: 'small',
  hitDie: 6 }, { name: 'medium',
  hitDie: 8 }, { name: 'large',
  hitDie: 10 }, { name: 'huge',
  hitDie: 12 }, { name: 'gargantuan',
  hitDie: 20 }];

exports.default = sizes;