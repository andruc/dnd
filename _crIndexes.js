"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/** Empty array to loop over in order to generate CR arrays */

var crIndexes = Array.apply(null, { length: 34 });

exports.default = crIndexes;