"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Converts CR to an integer index to ease computation
 */

var crToIndex = function crToIndex(cr) {
  return cr === 0 ? 0 : cr === 0.125 ? 1 : cr === 0.25 ? 2 : cr === 0.5 ? 3 : cr + 3;
};

exports.default = crToIndex;