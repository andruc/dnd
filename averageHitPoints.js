'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _averageRoll = require('./averageRoll');

var _averageRoll2 = _interopRequireDefault(_averageRoll);

var _abilityMod = require('./abilityMod');

var _abilityMod2 = _interopRequireDefault(_abilityMod);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Computes average hit points
 *
 * @param {string} hitDice Hit dice of a creature matching /\d^d(4|6|8|10|12|20)/
 * @param {number} constitution Ability score of a creature
 * @returns {number} Returns average hit points
 */

var averageHitPoints = function averageHitPoints(hitDice, constitution) {
  var _hitDice$split$map = hitDice.split('d').map(Number),
      _hitDice$split$map2 = _slicedToArray(_hitDice$split$map, 2),
      number = _hitDice$split$map2[0],
      size = _hitDice$split$map2[1];

  var baseHP = Math.floor(number * (0, _averageRoll2.default)(size));
  var bonusHP = number * (0, _abilityMod2.default)(constitution);

  return Math.max(1, baseHP + bonusHP);
};

exports.default = averageHitPoints;