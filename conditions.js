'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of every Condition in D&D
 */

var conditions = ['blinded', 'charmed', 'deafened', 'exhaustion', 'frightened', 'grappled', 'incapacitated', 'invisible', 'paralyzed', 'petrified', 'poisoned', 'prone', 'restrained', 'stunned', 'unconscious'];

exports.default = conditions;