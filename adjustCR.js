'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _crIndexes = require('./_crIndexes');

var _crIndexes2 = _interopRequireDefault(_crIndexes);

var _criToCR = require('./_criToCR');

var _criToCR2 = _interopRequireDefault(_criToCR);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
* A function that computes a creature's adjusted CR
*
* @param {number} cr A creature's CR in decimal form
* @param {number} [adjustment=0] Steps to modify CR
* @returns {number} New Challenge Rating
*/

var adjustCR = function adjustCR(cr, adjustment) {
  var inc = adjustment || 0;

  var cri = (0, _crIndexes2.default)(cr);

  // don't let the index go negative
  var adjustedCRI = Math.max(cri + inc, 0);

  return (0, _criToCR2.default)(adjustedCRI);
};

exports.default = adjustCR;