"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Expected Damage per round by Challenge Rating
 */

var damagePerRoundByCR = [[0, 1], [2, 3], [4, 5], [6, 8], [9, 14], [15, 20], [21, 26], [27, 32], [33, 38], [39, 44], [45, 50], [51, 56], [57, 62], [63, 68], [69, 74], [75, 80], [81, 86], [87, 92], [93, 98], [99, 104], [105, 110], [111, 116], [117, 122], [123, 140], [141, 158], [159, 176], [177, 194], [195, 212], [212, 230], [231, 248], [249, 266], [267, 284], [285, 302], [303, 320]];

exports.default = damagePerRoundByCR;