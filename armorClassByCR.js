"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A function that computes expected Hit Points from
 * Challenge Rating
 */

var armorClassByCR = function armorClassByCR(i) {
  var cri = i + 1;

  return Number(cri <= 7 ? 13 : cri <= 8 ? 14 : cri <= 11 ? 15 : cri <= 13 ? 16 : cri <= 16 ? 17 : cri <= 20 ? 18 : 19);
};

exports.default = armorClassByCR;