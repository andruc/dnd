"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A function that computes the expected proficiency for
 * a given challenge rating
 *
 * @param {number} cr A Challenge Rating INDEX
 * @return {number} Expected proficiency
 * @example
 *
 * proficiencyByCR(1) // CR 1/8
 * // => 2
 *
 * proficiencyByCR(12) // CR 8
 * // => 4
 */

var proficiencyByCR = function proficiencyByCR(i) {
  var cri = i + 1;

  return Number(cri <= 7 ? 2 : cri <= 11 ? 3 : cri <= 15 ? 4 : cri <= 19 ? 5 : cri <= 23 ? 6 : cri <= 27 ? 7 : cri <= 31 ? 8 : 9);
};

exports.default = proficiencyByCR;