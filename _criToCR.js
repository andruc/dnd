"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A function to convert CR Index back into useful CR
 */

var criToCR = function criToCR(i) {
  var cri = i + 1;

  return cri <= 1 ? 0 : cri <= 2 ? 0.125 : cri <= 3 ? 0.25 : cri <= 4 ? 0.5 : cri - 4;
};

exports.default = criToCR;