'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of every type of sense
 */

var senses = ['blindsight', 'darkvision', 'tremorsense', 'truesight'];

exports.default = senses;