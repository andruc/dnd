'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of resistances that a creature may have
 * against particular weapons
 */

var weaponResistances = ['bludgeoning, piercing, and slashing from nonmagical attacks', 'bludgeoning, piercing, and slashing from nonmagical attacks not made with silvered weapons', 'bludgeoning, piercing, and slashing from nonmagical attacks not made with adamantine weapons'];

exports.default = weaponResistances;