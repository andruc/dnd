'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A collection of functions that compute effective hit point
 * increases for various traits in the SRD
 *
 * Exports a higher-order function called to look up the
 * actual hit point increase
 *
 * @param {object} creature A creature object
 * @returns {function} Returns a function that takes a trait and
 *  returns a number
 *
 * @param {object} feature A trait or action object
 * @returns {number} Returns an effective hit point value
 */

/**
 * Increase the monster's effective hit points by 25% if the
 * monster is meant to face characters of 10th level or lower
 *
 * @returns {number} Effective hit point increase
 */
var frightfulPresenceBuff = function frightfulPresenceBuff(cr, hp) {
  return cr > 10 ? 0 : hp / 4;
};

/**
 * Each per-day use of this trait increases the monster's
 * effective hit points based on the expected challenge rating:
 * 1-4, 10 hp; 5-10, 20 hp; 11 or higher, 30 hp.
 *
 * @param {number} cr The creature's challenge rating
 * @param {number} [uses=1] The number of uses per-day
 * @returns {number} Effective hit point increase
 */
var _legendaryResistance = function _legendaryResistance(cr) {
  var uses = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  return uses * (cr < 5 ? 10 : cr < 11 ? 20 : 30);
};

/**
 * Increase the monster's effective hit points base on the
 * expected challenge rating: 1-4, 7 hp; 5-10, 14 hp;
 * 11-16, 21 hp; 17 or higher, 28 hp.
 *
 * @param {number} cr The creature's challenge rating
 * @returns {number} Effective hit point increase
 */

var _relentless = function _relentless(cr) {
  return cr < 5 ? 7 : cr < 11 ? 14 : cr < 17 ? 21 : 28;
};

/**
 * Increase the monster's effective hit points by 100%
 *
 * @param {number} hitPoints The creature's maximum hit points
 * @returns {number} Effective hit point increase
 */

var doubleHp = function doubleHp(hitPoints) {
  return hitPoints;
};

/**
 * Increase the monster's effective hit points by 3 x the number
 * of hit points the monster regenerates each round.
 *
 * @param {string} trait The trait's description
 * @returns {number} Effective hit point increase
 */

var _regeneration = function _regeneration(trait) {
  return 3 * Number(trait.split('regains ')[1].match(/(\d+)(?= hit points)/).pop());
};

/**
 * An object that keys trait names to calculations
 */
var effectiveHpCalculations = {
  'frightful presence': function frightfulPresence(c) {
    return frightfulPresenceBuff(c.challenge_rating, c.modified_hit_points || c.hit_points);
  },
  'horrifying visage': function horrifyingVisage(c) {
    return frightfulPresenceBuff(c.challenge_rating, c.modified_hit_points || c.hit_points);
  },
  'legendary resistance': function legendaryResistance(c) {
    return _legendaryResistance(c.challenge_rating);
  },
  'legendary resistance (3/day)': function legendaryResistance3Day(c) {
    return _legendaryResistance(c.challenge_rating, 3);
  },
  'possession': function possession(c) {
    return doubleHp(c.modified_hit_points || c.hit_points);
  },
  'relentless': function relentless(c) {
    return _relentless(c.challenge_rating);
  },
  'undead fortitude': function undeadFortitude(c) {
    return _relentless(c.challenge_rating);
  },
  'regeneration': function regeneration(c, t) {
    return _regeneration(t.desc);
  }

  /**
   * A higher-order function that encloses a creature object
   * allowing computation of a trait's effective hit point modifier
   */
};var effectiveHPCatalog = function effectiveHPCatalog(creature) {
  return function (feature) {
    var name = feature.name.toLowerCase();
    var calc = effectiveHpCalculations[name];

    if (feature.effect && feature.effect.hp) {
      return feature.effect.hp;
    }

    return calc ? calc(creature, feature) : 0;
  };
};

exports.default = effectiveHPCatalog;