'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _signModifier = require('./signModifier');

var _signModifier2 = _interopRequireDefault(_signModifier);

var _abilityMod = require('./abilityMod');

var _abilityMod2 = _interopRequireDefault(_abilityMod);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Returns a signed integer as a string given an ability
 * score
 *
 * @param {number} score The ability score to display
 * @returns {string} Retuns the appropriate modifier
 */

var signAbilityScore = function signAbilityScore(score) {
  return (0, _signModifier2.default)((0, _abilityMod2.default)(score));
};

exports.default = signAbilityScore;