'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A (albeit incomplete) list of langauges in D&D
 */

var languages = ['common', 'dwarvish', 'elvish', 'giant', 'gnomish', 'goblin', 'halfling', 'orc', 'abyssal', 'celestial', 'draconic', 'deep speech', 'infernal', 'primordial', 'sylvan', 'undercommon'];

exports.default = languages;