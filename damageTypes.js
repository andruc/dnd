'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A list of damage/energy types in D&D
 */

var damageTypes = ['acid', 'bludgeoning', 'cold', 'fire', 'force', 'lightning', 'necrotic', 'piercing', 'poison', 'psychic', 'radiant', 'slashing', 'thunder'];

exports.default = damageTypes;